﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ShopWebApp.DataAccessLayer.Interface;
using ShopWebApp.Model.Entities;

namespace ShopWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FruitsController : ControllerBase
    {
        private IUnitOfWork unitOfWork;

        public FruitsController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status302Found)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<Fruit> GetAll()
        {
            return unitOfWork.FruitRepo.GetAll();
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status302Found)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Fruit> GetById(int id)
        {
            var fruit = unitOfWork.FruitRepo.GetById(id);
            if (fruit == null) return NotFound();

            return fruit;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Fruit> Create(Fruit fruit)
        {
            unitOfWork.FruitRepo.Insert(fruit);
            unitOfWork.Save();

            return CreatedAtAction(nameof(GetById), new { id = fruit.Id }, fruit);
        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Fruit> Update(Fruit fruit)
        {
            unitOfWork.FruitRepo.Update(fruit);
            unitOfWork.Save();

            return CreatedAtAction(nameof(GetById), new { id = fruit.Id }, fruit);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Delete(int id)
        {
            var fruit = unitOfWork.FruitRepo.GetById(id);

            if (fruit == null)
            {
                return NotFound();
            }

            unitOfWork.FruitRepo.Delete(fruit);
            unitOfWork.Save();

            return NoContent();
        }
    }
}
