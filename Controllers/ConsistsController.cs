﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ShopWebApp.DataAccessLayer.Interface;
using ShopWebApp.Model.Entities;

namespace ShopWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConsistsController : ControllerBase
    {
        private IUnitOfWork unitOfWork;

        public ConsistsController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status302Found)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<Consist> GetAll()
        {
            return unitOfWork.ConsistRepo.GetAll();
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status302Found)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Consist> GetById(int id)
        {
            var consist = unitOfWork.ConsistRepo.GetById(id);
            if (consist == null) return NotFound();

            return consist;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Consist> Create(Consist consist)
        {
            unitOfWork.ConsistRepo.Insert(consist);
            unitOfWork.Save();

            return CreatedAtAction(nameof(GetById), new { id = consist.Id }, consist);
        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Consist> Update(Consist consist)
        {
            unitOfWork.ConsistRepo.Update(consist);
            unitOfWork.Save();

            return CreatedAtAction(nameof(GetById), new { id = consist.Id }, consist);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Delete(int id)
        {
            var consist = unitOfWork.ConsistRepo.GetById(id);

            if (consist == null)
            {
                return NotFound();
            }

            unitOfWork.ConsistRepo.Delete(consist);
            unitOfWork.Save();

            return NoContent();
        }
    }
}
